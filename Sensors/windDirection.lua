local sensorInfo = {
	name = "windDirection",
	desc = "Return wind direction scaled to 300x normalized distance.",
	author = "Ondřej Novák",
	date = "2020-05-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind direction
return function()
	local _, _, _, _, normDirX, _, normDirZ = SpringGetWind()

	return { x = 300 * normDirX, z = 300 * normDirZ }
end
