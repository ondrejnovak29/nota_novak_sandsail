function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Orders a transport unit to move to a given location.",
		parameterDefs = {
			{
				name = "location",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetGroundHeight = Spring.GetGroundHeight


-- @description horizontal euclidean distance between 2 points
function euclDist(x1, z1, x2, z2)
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
end

local function ClearState(self)
	self.target = nil
end

function Run(self, unitIds, p)
	if unitIds[1] == nil then
		return FAILURE
	end

	local x, _, z = SpringGetUnitPosition(unitIds[1])

	if self.target == nil then
		if p.location.y == nil then
			local tx, tz = p.location.x + x, p.location.z + z
			self.target = {x=tx, y=SpringGetGroundHeight(tx, tz), z=tz}
		else
			self.target = p.location
		end
		
		for _, uid in ipairs(unitIds) do
			SpringGiveOrderToUnit(
				uid,
				CMD.MOVE,
				{self.target.x, self.target.y, self.target.z},
				{}
			)
		end
	end

	if euclDist(self.target.x, self.target.z, x, z) < 100 then
		return SUCCESS
	else
		return RUNNING
	end
end

function Reset(self)
	ClearState(self)
end
